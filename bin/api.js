var restify = require('restify');
var server = restify.createServer();
var models = require('../model/models');
var config = require('getconfig');

models.sequelize.sync();

server.use(restify.fullResponse())
  .use(restify.bodyParser())
  .use(restify.queryParser());

// server.pre(function (request, response, next) {
//   models.Utilisateur.find({where: {apiKey: request.header('X-Api-Key')}}).complete(function(err, Utilisateur) {
//     console.log( request.header('X-Api-Key'));
//     if (!!err) {
//       console.log('An error occurred while fetching Utilisateur', err)
//     } else if (!Utilisateur) {
//       return next(new restify.UnauthorizedError('utilisateur non trouvé'));
//     } else {
//       return next();
//     }
//   });
// });


require('../api/routes/color')(server);
require('../api/routes/book')(server);
require('../api/routes/automatique')(server);


server.listen(config.apiPort, function() {
  console.log('%s listening at %s...', server.name, server.url);
});
