var http = require('http');
var httpProxy = require('http-proxy');
var config = require('getconfig');

var proxy = httpProxy.createProxyServer({});


var server = http.createServer(function(req, res) {
  // You can define here your custom logic to handle the request
  // and then proxy the request.
  var entrypoint = req.url.split('/')[1];
  console.log(entrypoint);
  if (entrypoint === 'api') {
    var url = 'http://localhost:' + config.apiPort;
  } else {
    var url = 'http://localhost:' + config.appPort;
  }

  proxy.web(req, res, { target: url });
});

server.listen(80);
console.log("listening on port 80")
