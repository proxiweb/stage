var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var stylish = require('jshint-stylish');
var buffer = require('vinyl-buffer');
var _ = require('lodash');
var uglify = require('gulp-uglify');
var del = require('del');

var browserSync = require('browser-sync');
var reload = browserSync.reload;

//var app = require('./bin/app');

gulp.task('clean', function(cb) {
    del([
	'app/tmp'
    ], cb);
});

gulp.task('html', function() {
  return gulp.src('./src/index~.html')
    .pipe($.plumber())
    .pipe(gulp.dest('./dist'));
});


gulp.task('rename',function(){
 $.run('./rename.sh').exec();
});

gulp.task('styles', function() {
  return gulp.src('./src/main.scss')
    .pipe($.sass({errLogToConsole: true}))
    .pipe($.autoprefixer())
    .pipe($.rename('bundle.css'))
    .pipe(gulp.dest('./dist'))
    .pipe(reload({ stream: true }));
});

var bundler = _.memoize(function() {
  return watchify(browserify('./src/main.js', _.extend({ debug: true }, watchify.args)));
});

function bundle() {
  return bundler().bundle()
    .on('error', handleErrors)
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe($.sourcemaps.init({ loadMaps: true }))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('./dist'))
    .pipe(reload({ stream: true }));
}

gulp.task('scripts', function() {
  process.env.BROWSERIFYSWAP_ENV = 'dist';
  return bundle();
});

gulp.task('jshint', function() {
  return gulp.src(['./src/**/*.js', './test/**/*.js'])
    .pipe($.plumber())
    .pipe($.jshint({browser:true}))
    .pipe($.jshint.reporter(stylish));
});

gulp.task('uglify',function(){
    return gulp.src('./dist/bundle.js')
        .pipe(uglify('bundle.min.js'))
        .pipe(gulp.dest('./dist/'));
});

var reporter = 'spec';

gulp.task('mocha', ['jshint'], function() {
  return gulp.src([
    './test/setup/node.js',
    './test/setup/helpers.js',
    './test/unit/**/*.js'
  ], { read: false })
    .pipe($.plumber())
    .pipe($.mocha({ reporter: reporter }));
});

gulp.task('build', [
  'clean',
  'html',
  'styles',
  'scripts',
  'test'
]);

gulp.task('test', [
  'jshint',
  'mocha'
]);

gulp.task('watch', ['build'], function() {
  //browserSync({
  //  server: {
  //    baseDir: 'dist',
  //    middleware: function(req, res, next) {
  //      app(req, res, next);
  //    }
  //  }
  //});

  reporter = 'dot';
  bundler().on('update', function() {
    gulp.start('scripts');
    gulp.start('test');
  });
  gulp.watch('./test/**/*.js', ['test']);
  gulp.watch(['./src/main.scss', './src/**/*.scss'], ['styles']);
});

var handleErrors = function() {
  var args = Array.prototype.slice.call(arguments);
  delete args[0].stream;
  $.util.log.apply(null, args);
  this.emit('end');
};

gulp.task('default', ['watch']);
