var restify = require('restify');
var models = require('../../model/models');
var underscored = require('underscore.string/underscored');
var a = require('../authRules');


module.exports = function (server) {

var defaultAuthRules = {
	post: 'isAdmin',
	del: 'isAdmin',
	put: 'isAdmin',
	getAll: 'forAll',
	getOne: 'forAll'
};  

var mapRequest = function(request, modelName) {
	var attributs = {};

	Object.keys(models[modelName].rawAttributes).forEach(function(attribute) {

		if (attribute === 'id') {
			return;
		}

		if (typeof request.body[attribute] !== 'undefined') {
			attributs[attribute] = request.body[attribute];
		}
	});

	return attributs;
};


var generateIncludes = function(modelName, query) {

	if (typeof models[modelName].options.includes === 'undefined') {
		return [];
	}

	if (typeof models[modelName].options.includes[query] === 'undefined' ) {
		return [];
	}

        var includes = [];
        models[modelName].options.includes[query].forEach(function(inc){
            if (inc.modelName === 'All') {
                    includes.push({all: true});
                    return;
            }
            var toInclude = {model: models[inc.modelName]};
            
            if (typeof inc.include !== 'undefined') {
                    toInclude.include = [];
                    inc.include.forEach(function(inc2){
                            var toInclude2 = {model: models[inc2.modelName]};

                            if (typeof inc2.include !== 'undefined') {
                                    toInclude2.include = [];

                                    inc2.include.forEach(function(inc3) {
                                            toInclude2.include.push({model: models[inc3.modelName]});
                                    });
                            }

                            toInclude.include.push(toInclude2);
                    });        	
            }

            includes.push(toInclude);
    });

    return includes;
};

Object.keys(models.sequelize.models).forEach(function (modelName){

	if (typeof models[modelName] === 'undefined' || typeof models[modelName].options.automatique === 'undefined') {
		return;
	}

	var rules = typeof models[modelName].authRules === 'undefined' ?
				defaultAuthRules :
				models[modelName].authRules;

	var path = underscored(modelName);

	// détermination du pluriel
	var plur = typeof models[modelName].options.pluriel === 'undefined' ?
						's' :
						models[modelName].options.pluriel;


	console.log('creation de la route ' + path);

	server.get('/api/' + path + plur, a[rules.getAll], function (req, res, next) {
		models[modelName].findAll({
			include: generateIncludes(modelName,'getAll')
		}).then(function (rows) {
		  	var obj = {};
		  	obj[modelName + plur] = rows;
		    res.send(obj);
		    return next();
		}).catch(function(err){
			console.log('An error occurred while fetching ' + modelName, err);			
		});
	});
	// .fail(function(err){
	// });

	server.get('/api/' + path + plur + '/:id', a[rules.getOne], function (req, res, next) {
		models[modelName].find({
			where: {id: req.params.id},
			include: generateIncludes(modelName,'getOne') 
		}).then(function (row) {
		  if (!row) {
                res.send(new restify.NotFoundError('element non trouvé'));
                return next();
            } else {

		    res.send(row);
		    return next();
		  }
		}).error(function(){
					    console.log('An error occurred while fetching ' + modelName, err)

		});
	});	
	
	server.del('/api/' + path + plur + '/:id', a['forAll'], function (req, res, next) {
		models[modelName].find({where: {id: req.params.id}}).then(function (row) {

                    row.destroy().then(function () {
                        res.send(204);
                        return next();
                    }).error(function(err) {
                        console.log('An error occurred while fetching ' + modelName, err)
                        res.send(500);
                        return next();
                    });

		}).catch(function(err){
		    console.log('An error occurred while fetching ' + modelName, err)
                    res.send(500);
                    return next();
                });
	});	

    server.put('/api/' + path + plur + '/:id', a['forAll'],function(req, res, next) {
        models[modelName].find({where: {id: req.params.id}}).then(function(row) {
            if (!row) {
                res.send(new restify.NotFoundError('couleur non trouvé'));
                return next();
            } else {
                row.updateAttributes(mapRequest(req,modelName)).then(function() {
                    res.status(200);
                    res.send(row);
                    return next();
                });
            }
        }).catch(function(err){
            console.log('An error occurred while fetching ' + modelName, err)
            res.send(500);        
            return next();
        });
    });

	server.post('/api/' + path + plur, a['forAll'], function (req, res, next) {

		var newModel = models[modelName].build(mapRequest(req,modelName));

		newModel.save().then(function (result) {
			res.status(201);
			res.send(newModel);
			return next();
		}).catch(function(err){
                    console.log(err);
                    res.send(500);                    
                });
	});	

});


};
