var restify = require('restify');
var models = require('../../model/models');

module.exports = function (server) {

  server.get('/api/colors', function (req, res, next) {
    models.Color.findAll().then(function (colors) {
      res.send(colors);
      return next();    
    }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });


  server.get('/api/colors/:id', function (req, res, next) {
    models.Color.find({where: {id: req.params.id}}).then(function (color) {
      if (!color) {
        res.send(new restify.NotFoundError('couleur non trouvé'));
        return next();
      } else {
        res.send(color);
        return next();
      }
    }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });

  server.del('/api/colors/:id', function (req, res, next) {
    models.Color.find({where: {id: req.params.id}}).then(function (color) {
      if (!color) {
        res.send(new restify.NotFoundError('couleur non trouvé'));
        return next();
      } else {
        color.destroy().then(function () {
          res.send(204);
          return next();
        });
      }
    }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });

  server.put('/api/colors/:id', function (req, res, next) {
    models.Color.find({where: {id: req.params.id}}).then(function (color) {
      if (!color) {
        res.send(new restify.NotFoundError('couleur non trouvé'));
        return next();
      } else {
        color.updateAttributes(
          {
            name: req.body.name,
            hex: req.body.hex
          }
        ).then(function () {
            res.status(200);
            res.send(color);
            return next();
          });
      }
    }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });

  server.post('/api/colors', function (req, res, next) {
    var newColor = models.Color.build(
      {
        name: req.body.name,
        hex: req.body.hex
      }
    );

    newColor.save().then(function (result) {
      if (err) {
        console.log(err);
        res.send(500);
      }
      res.status(201);
      res.send(newColor);
      return next();
    }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });
};
