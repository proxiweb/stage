var restify = require('restify');
var models = require('../../model/models');

module.exports = function (server) {

  server.get('/api/books', function (req, res, next) {
    models.Book.findAll()
      .then(function (books) {
        res.send(books);
        next();
      }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });

  server.get('/api/books/:id', function (req, res, next) {
    models.Book.find({where: {id: req.params.id}}).then(function (book) {
      if (!book) {
        res.send(new restify.NotFoundError('livre non trouvé'));
        next();
      } else {
        res.send(book);
        next();
      }
    }).error(function(error) {
            res.status(500);
            res.send(error);
            return next();      
    });
  });

};
