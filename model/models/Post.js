"use strict";
module.exports = function (sequelize, DataTypes) {
  var Post = sequelize.define("Post", {
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    message: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: true
      }
    },
    author: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  }, {
    automatique : true,
    classMethods: {
      associate: function (models) {
        // associations can be defined here
      }
    }
  });
  return Post;
};
