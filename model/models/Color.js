"use strict";
module.exports = function (sequelize, DataTypes) {
  var Color = sequelize.define("Color", {
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    hex: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  }, {
    classMethods: {
      associate: function (models) {
        // associations can be defined here
      }
    }
  });
  return Color;
};
