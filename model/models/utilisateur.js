"use strict";
module.exports = function(sequelize, DataTypes) {
  var Utilisateur = sequelize.define("Utilisateur", {
    pseudo: DataTypes.STRING,
    motDePasse: DataTypes.STRING,
    email: DataTypes.STRING,
    actif: DataTypes.BOOLEAN,
    superAdmin: DataTypes.BOOLEAN,
    lastLogin: DataTypes.DATE,
    apiKey: DataTypes.STRING,
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  });
  return Utilisateur;
};
