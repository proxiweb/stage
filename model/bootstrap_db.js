var sequelize_fixtures = require('sequelize-fixtures');
var sequelize = require('sequelize');
var models = require('./models');

models.sequelize.sync({force:true}).then(function() {
  sequelize_fixtures.loadFile('./fixtures/book.json', models, function(){
    sequelize_fixtures.loadFile('./fixtures/color.json', models, function(){
	    sequelize_fixtures.loadFile('./fixtures/post.json', models, function(){
	      process.exit(0);
	    });
    });
  });
});
