import View from '../../common/view';
import FormBehavior from '../../forms/behavior';
import Backbone from 'backbone';
import template from './template.hbs';
import _ from 'lodash';

export default View.extend({
  template: template,

  className: 'colors colors--create container',

  behaviors: {
    form: {behaviorClass: FormBehavior}
  },

  templateHelpers() {
    return {
      errors: this.errors
    };
  },

  initialize() {
    _.bindAll(this, 'handleSaveSuccess');
  },

  events: {
    'submit form': 'handleSubmit'
  },

  handleSubmit() {
    var errors = this.model.validate(this.form);

    if (!errors) {
      this.model.save(this.form).done(this.handleSaveSuccess);
    } else {
      this.errors = errors;
      this.render();
    }
  },

  handleSaveSuccess() {
    this.collection.add(this.model);
    Backbone.history.navigate('colors', {trigger: true});
  }
});
