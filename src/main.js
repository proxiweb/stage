import './plugins';
import Backbone from 'backbone';

import Application from './application/application';

import ModalService from './modal/service';
import HeaderService from './header/service';
import FlashesService from './flashes/service';

import IndexRouter from './index/router';
import ColorsRouter from './colors/router';
import BooksRouter from './books/router';

import PostRouter from "./posts/router";

(function() {
  var proxiedSync = Backbone.sync;

  Backbone.sync = function(method, model, options) {
    options = options || (options = {});

    if (typeof options.crossDomain === 'undefined') {
      options.crossDomain = true;
    }
    options.headers = {
      Accept: 'application/json',
      'X-Api-Key': window.user.apiKey
    };
    return proxiedSync(method, model, options);
  };
})();

let app = new Application();

app.modal = new ModalService({
  container: app.layout.overlay
});

app.header = new HeaderService({
  container: app.layout.header
});

app.flashes = new FlashesService({
  container: app.layout.flashes
});

app.index = new IndexRouter({
  container: app.layout.content
});

app.colors = new ColorsRouter({
  container: app.layout.content
});

app.books = new BooksRouter({
  container: app.layout.content
});

app.post = new PostRouter({
  container: app.layout.content
});

Backbone.history.start();
