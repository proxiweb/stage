import $ from 'jquery';
import _ from 'lodash';
import Radio from 'backbone.radio';
import Application from '../common/application';
import LayoutView from './layout-view';

let routerChannel = Radio.channel('router');

export default Application.extend({
  initialize() {
    this.$body = $(document.body);
    this.layout = new LayoutView();
    this.layout.render();

    this.listenTo(routerChannel, {
      'before:enter:route' : this.onBeforeEnterRoute,
      'enter:route'        : this.onEnterRoute,
      'error:route'        : this.onErrorRoute
    });
  },

  onBeforeEnterRoute() {
    this.transitioning = true;
    // Don't show for synchronous route changes
    _.defer(() => {
      if (this.transitioning) {
        $('#spin-wrap').removeClass('hide');
      }
    });
  },

  onEnterRoute() {
    this.transitioning = false;
    this.$body.scrollTop(0);
    $('#spin-wrap').addClass('hide');
  },

  onErrorRoute() {
    this.transitioning = false;
    $('#spin-wrap').addClass('hide');
  }
});
