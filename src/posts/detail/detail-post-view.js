import ItemView from '../../common/item-view';
import template from './detail-template.hbs';

export default ItemView.extend({
  template: template,
  className: 'preview',

  triggers: {
    'click' : 'post:edit'
  },

  modelEvents: {
    'all' : 'render'
  }
});
