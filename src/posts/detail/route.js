import Route from '../../common/route';
import DetailLayout from './detail-layout';
import CollectionView from '../index/collection-view';
import storage from '../storage';
import $ from 'jquery';

export default Route.extend({
  initialize(options) {
    this.layout = options.container;
  },

  fetch(id) {
    return storage.find(id).then(model => {
      this.model = model;
    });
  },

  render() {
    var self = this;
    
    //$('.posts').addClass('detail_tab');
    storage.findAll().then(collection => {
      var collectionView = new CollectionView({
        collection: collection
      });
      
      self.layout.postslist.show(collectionView);
    })

    console.log(this.model);

    var detailLayout = new DetailLayout({model: this.model});
    this.layout.postdetails.show(detailLayout);

    
    // collectionView.on('childview:post:selected', (childview, args) => {
    //   alert('ok');
    // })

    // this.container.show(layout);
  }
});
