import LayoutView from '../../common/layout-view';
import DetailPostView from './detail-post-view';
import DetailPostEdit from './detail-edit-view';
import template from './detail-layout-template.hbs';
import $ from 'jquery';

export default LayoutView.extend({
	template: template,
	regions: {
		postPreview: '#post-preview',
		postEdit: '#post-edit'
	},

	onAttach() {
		var self = this;
		this.detailPostView = new DetailPostView({model: this.model});
		this.postPreview.show(this.detailPostView);

		this.detailPostEdit = new DetailPostEdit({model: this.model});
		this.postEdit.show(this.detailPostEdit);

		this.detailPostView.on('post:edit', function() {
			$('.post_details').addClass('editing');
		});

		this.detailPostEdit.on('post:save', function(args) {

			args.model.save({
				title: $('.edit .post-title').text(),
				message: $('.edit .post-message').text()
			},{
				success(post, response, options) {
					self.model = post;
				}
			});

			$('.post_details').removeClass('editing');
		});

	}
});