import ItemView from '../../common/item-view';
import template from './detail-edit-template.hbs';

export default ItemView.extend({
  template: template,
  className: 'edit',
  triggers: {
    'click #save-post': 'post:save'
  },
  modelEvents: {
    'all' : 'render'
  }
});
