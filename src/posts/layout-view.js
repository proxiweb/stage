import _ from 'lodash';
import LayoutView from '../common/layout-view';
import CollectionView from './index/collection-view';
import DetailLayout from './detail/detail-layout';
import template from './layout-template.hbs';
import storage from './storage';

export default LayoutView.extend({
	template: template,

	className: 'posts',

	regions: {
		postslist: '.posts_list',
		postdetails: '.post_details'
	},

	onAttach() {
		var self = this;
		storage.findAll().then(collection => {

			self.collection = collection;

		    self.collectionView = new CollectionView({
		      collection: this.collection
		    });

		    self.collectionView.on('childview:post:selected', function(view, args) {

		    	var detailLayout = new DetailLayout({model: args.model});
		    	self.postdetails.show(detailLayout);
		    	self.$el.addClass('detail_tab');

		    });

			// self.postsListView = new PostsListView(collectionView);
			self.postslist.show(self.collectionView);

		});
	}

	// onPostSelected() {
	// 	var model = this.collection.findWhere({id: id});
	// }

});