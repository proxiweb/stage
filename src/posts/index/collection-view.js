import CollectionView from '../../common/collection-view';
import ItemView from './postslist-view';

export default CollectionView.extend({
  className: 'list-group',
  childView: ItemView
});
