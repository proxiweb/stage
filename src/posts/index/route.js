import Route from '../../common/route';
import LayoutView from '../layout-view';
import CollectionView from './collection-view';
import storage from '../storage';

export default Route.extend({
  initialize(options) {
    this.layout = options.container;
    this.collection = options.collection;
  }
});
