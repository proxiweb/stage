import ItemView from '../../common/item-view';
import template from './postslist-template.hbs';

export default ItemView.extend({
  template: template,
  className: 'post',
  templateHelpers() {
    // var titleB = this.options.uppercase ?
    console.log("test", this.model);
    return {
      short_desc : this.model.get("message").substr(0,140)+" ..."
    }
  },

  triggers: {
  	'click' : 'post:selected'
  }
});
