import Router from '../common/router';
import Radio from 'backbone.radio';
import IndexRoute from './index/route';
import DetailRoute from './detail/route';
import LayoutView from './layout-view';

export default Router.extend({
  initialize(options) {
    this.container = options.container;
    Radio.command('header', 'add', {
      name: 'Posts',
      path: 'posts',
      type: 'primary'
    });    
  },

  onBeforeEnter() {
    Radio.command('header', 'activate', {path: 'posts'});
    this.layout = new LayoutView();
    this.container.show(this.layout);
  },

  routes: {
    'posts': 'index',
    'posts/:id': 'detail'
  },

  index() {
    return new IndexRoute({
      container: this.layout
    });
  },

  detail() {
    return new DetailRoute({
      container: this.layout
    });
  }
});
