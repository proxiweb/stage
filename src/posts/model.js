import Model from '../common/model';

export default Model.extend({
  urlRoot: '/api/posts'
});
