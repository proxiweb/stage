import Collection from '../common/collection';
import Model from './model';

export default Collection.extend({
  url: '/api/posts',
  parse(response) {
  	return response.Posts;
  },
  model: Model
});
