var LocalStrategy   = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var Models = require('../model/models/index');
var uuid = require('node-uuid');

module.exports = function(passport){

    passport.use('signup', new LocalStrategy({
                passReqToCallback : true // allows us to pass back the entire request to the callback
            },
            function(req, username, password, done) {

                findOrCreateUser = function(){
                    // find a user in Mongo with provided username

                    Models.Utilisateur.find({where: {pseudo: username}}).then( function (utilisateur){

                        if (utilisateur) {
                            return done(null, false, req.flash('message','Cet utilisateur existe déjà'));
                        } else {
                            var nouvelUtilisateur = Models.Utilisateur.build({
                                pseudo: username,
                                motDePasse: createHash(password),
			                    apiKey: uuid.v4()
                            });

                            nouvelUtilisateur.save().then(function(){
                                return done(null, nouvelUtilisateur);
                            }).error(function(err){
                                console.log('Error in Saving user: ' + err);
                                throw err;
                            });
                        }
                    }).error(function(err){
                        console.log('Error in SignUp: ' + err);
                        return done(err);
                    });
                };

                // Delay the execution of findOrCreateUser and execute the method
                // in the next tick of the event loop
                process.nextTick(findOrCreateUser);
            })
    );

    // Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };

};
