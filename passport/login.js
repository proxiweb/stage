var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var Models = require('../model/models/index');

module.exports = function(passport) {

    passport.use('login', new LocalStrategy({
                passReqToCallback : true
            },
            function (req, username, password, done) {
                // check in mongo if a user with username exists or not
                Models.Utilisateur.find({where: {pseudo: username}}).then(function (utilisateur) {
		
                    if (!utilisateur) {
                        return done(null, false, req.flash('message', 'Cet utilisateur n\'existe pas.'));
                    }                   
            
                    if (!isValidPassword(utilisateur, password)) {
                        return done(null, false,req.flash('message', 'Mot de passe non valide'));
                    }
//                     // User and password both match, return user from
//                     // done method which will be treated like success
                    return done(null, utilisateur.toJSON());
                }).error(function(err){
                    return done(err);
                });
            }
    ));

    var isValidPassword = function(utilisateur, password)  {
      try {
        var res = bCrypt.compareSync(password, utilisateur.motDePasse);
      } catch (e) {
	return false;
      }
      return res;
    };

};
